# README #
This visualization shows the beauty of Pi. The project is an adaptation of Martin Krzywinski's art (http://mkweb.bcgsc.ca/pi/art/)

### Get Started ###
- Clone the reposity
- cd into the repository
- Open index.html with Google Chrome

![alt tag](https://bytebucket.org/paolo_bruzzo/pi-is-beautiful/raw/bd6066fd971f694c7fad5f7d9d63c9e5778ff6a3/screenshots/Capture.JPG)