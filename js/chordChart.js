var ChordChart = function () {
    var self = this;
    /* ===================================================
     *  ATTRIBUTES
     * =================================================== */
    var _number; // Pi, e ...
    var _allData; // The decimal digits
    var _digits; // Number of digits to show
    var _defaultDigits = 100000;
    var _svg;
    var _g;

    /* ===================================================
     *  PUBLIC METHODS
     * =================================================== */

    /**
     * Reads the input value of decimal digits, and returns the matching digits subset
     * @returns [*]
     */
    this.getDataToDisplay = function () {
        _digits = $('#digits').val();
        if( ! _digits)
            _digits = _defaultDigits;
        if(_digits > 0 && _digits < _allData.length)
            return _allData.slice(0, _digits);
        return _allData;
    };

    /**
     * Deletes and rebuild the chord chart, based on the digitsToShow. If the latter is null, redraws the default chart
     * @param digitsToShow
     */
    this.refreshViz = function (digitsToShow) {
        d3_delete_from(_svg);
        if(! digitsToShow)
            digitsToShow = self.getDataToDisplay().length;
        if(digitsToShow > 0)
            _generateViz(digitsToShow);
    };

    /**
     * Progressivily refresh the chart from 0 to N digits, speeding up at each iteration
     */
    this.runAnimation = function () {
        d3_enable_buttons(false);
        var max = self.getDataToDisplay().length;
        var msToWait = 1000;
        self.refreshViz(0);
        (function myLoop (i) {
            setTimeout(function () {
                if (i < max){
                    self.refreshViz(i);
                    if(i < 100){
                        i+= 1; msToWait/=1.05;
                    } else {
                        if( i < 1500){
                            i+= 10; msToWait/=1.1;
                        }
                        else
                            i += 1000;
                    }
                    myLoop(i);
                }
                else {
                    self.refreshViz(max);
                    d3_enable_buttons(true);
                }
            }, msToWait)
        })(1);
    };

    /* ===================================================
     *  PRIVATE METHODS
     * =================================================== */
    var _loadData = function (filepath, jsonId) {
        _allData = [];
        d3.json(filepath, function (error, json) {
            if(error){
                console.error("Error in loading "+filepath);
                return ;
            }
            var decimals = json[jsonId];
            for(var i =0, len = decimals.length; i < len - 1; i++){
                _allData.push([decimals[i], decimals[i+1], 1]);
            }
            notificationCenter.dispatch(EVENT.PI_LOADED);
        });
    };

    var _generateViz = function (digitsToShow) {
        if( ! digitsToShow)
            digitsToShow = _defaultDigits;
        var visual = viz.ch().data(self.getDataToDisplay().slice(0, digitsToShow))
            .padding(.1)
            .innerRadius(435)
            .outerRadius(450)
            .duration(1000)
            .chordOpacity(0.8)
            .labelPadding(.02)
            .startAngle(-1.85)
            .fill(function(d){ return COLOR[d];});
        d3_add_text(_svg, "Digits Shown: "+digitsToShow.toString());
        _g = d3_append_g(_svg).call(visual);
    };

    /* ===================================================
     *  CONSTRUCTOR
     * =================================================== */
    var _init = function () {
        notificationCenter.subscribe(self, EVENT.PI_LOADED, _generateViz);
        notificationCenter.subscribe(self, EVENT.RUN_ANIMATION, self.runAnimation);
        notificationCenter.subscribe(self, EVENT.CALCULATE_FINAL_CHORD, self.refreshViz);

        _allData = [];
        _digits = _defaultDigits;
        _svg = d3_append_svg("animationContainer");
        d3_enable_buttons(true);
        _loadData("data/pi_decimals.json", "pi_decimals");
    }();
};