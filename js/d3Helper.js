var d3_select_id = function(id) {
    return d3.select('#'+id);
};

var d3_append_svg = function (container) {
    return d3_select_id(container).append("svg").attr("width", "100%").attr("height", "100%").attr("viewBox", "0 0 1580 980").attr("preserveAspectRatio","xMinYMid meet");
};

var d3_append_g = function (svg) {
    return svg.append("g").attr("transform", "translate(790,490)");
};

var d3_delete_from = function (d3Elem) {
    d3Elem.selectAll("*").remove();
};

var d3_add_text = function (container, text) {
    return container.append("text").attr("x", 10).attr("y", 35).attr("class", "cooltext").text(text);
};

var d3_enable_buttons = function (enabled) {
    var animationButton = $('#animationButton');
    var calculateButton = $('#calculateButton');
    if(enabled){
        animationButton.attr('onclick', "notificationCenter.dispatch(EVENT.RUN_ANIMATION)");
        calculateButton.attr('onclick', "notificationCenter.dispatch(EVENT.CALCULATE_FINAL_CHORD)");
    }
    else {
        animationButton.removeAttr('onclick');
        calculateButton.removeAttr('onclick');
    }
    animationButton.attr('disabled', !enabled);
    calculateButton.attr('disabled', !enabled);
};