/**
 * Created by Paolo Bruzzo on 21-Jun-16.
 *
 * This class models a publish-subscribe pattern.
 */
var NotificationCenter = function () {

    /* ===================================================
     *  ATTRIBUTES
     * =================================================== */
    var _subscribers;

    /* ===================================================
     *  PUBLIC METHODS
     * =================================================== */

    /**
     * Subscribes a given subscriber to a certain event. The callback function will be called when dispatch will be invoked.
     * @param subscriber
     * @param event
     * @param callback
     */
    this.subscribe = function (subscriber, event, callback) {
        // Check if the subscriber is already subscribed
        if (_isSubscribed(subscriber, event)) {
            console.error(subscriber, " already subscribed to ", event);
            return;
        }

        // Check if callback is a function
        if( ! callback || typeof callback != 'function'){
            console.error(callback, " is not a function");
            return;
        }

        // Initialize dictionary voice if it doesn't exist yet
        if (!_hasSubscribers(event))
            _subscribers[event] = [];

        // Add subscription
        _subscribers[event].push({
            subscriber: subscriber,
            callback: callback
        });

    };

    /**
     * Unuscribes a subscriber from a given event
     * @param subscriber
     * @param event
     */
    this.unsubscribe = function (subscriber, event) {
        // Return if there is not active subscription
        if (!_isSubscribed(subscriber, event)) {
            console.error(subscriber, " is not subscribed to ", event);
            return;
        }

        // Remove subscriber
        _subscribers[event].forEach(function (currentValue, index) {
            if (currentValue.subscriber === subscriber)
                _subscribers[event].splice(index, 1);
        });

        // Delete the entry if there are no more subscribers
        if (_subscribers[event].length == 0)
            delete _subscribers[event];

    };

    /**
     * Unsubscribes a certain subscriber from all the events it has been registered
     * @param subscriber
     */
    this.unsubscribeAll = function (subscriber) {

        for (var event in _subscribers) {
            if (!_subscribers.hasOwnProperty(event))
                continue;

            // Unuscribe if already subscribed
            if (_isSubscribed(subscriber, event))
                this.unsubscribe(subscriber, event);
        }

    };

    /**
     * Calls all the subscribed callbacks for a given event, and optional arguments
     * @param event
     * @param args
     */
    this.dispatch = function (event, args) {

        // Return if there is no subscriber
        if (!_hasSubscribers(event))
            return;

        // Fire error if args exists but i not an array
        if( args && ! Array.isArray(args)){
            console.error(args, " is not an array");
            return;
        }

        // Otherwise call methods
        _subscribers[event].forEach(function (subscriber) {
            subscriber.callback.apply(subscriber, args);
        });

    };

    /**
     * Return the list of all the subscriptions
     * @returns {*}
     */
    this.getSubscriptionsList = function () {
        return _subscribers;
    };

    /* ===================================================
     *  PRIVATE METHODS
     * =================================================== */

    /**
     * Returns true if an event has at least 1 subscriber
     * @param event
     * @returns {boolean}
     * @private
     */
    var _hasSubscribers = function (event) {
        return (typeof _subscribers[event] != 'undefined');
    };

    /**
     * Returns true if a certain subscriber is already subscribed to a certain event
     * @param subscriber
     * @param event
     * @returns {boolean}
     * @private
     */
    var _isSubscribed = function (subscriber, event) {
        // Return false if the event doesn't have any subscriber
        if (!_hasSubscribers(event))
            return false;

        // If it does, search for the subscriber
        for (var i in _subscribers[event]) {
            if (!_subscribers[event].hasOwnProperty(i))
                continue;

            // If found
            if (_subscribers[event][i].subscriber === subscriber)
                return true;
        }

        // If not found
        return false;
    };

    /* ===================================================
     *  CONSTRUCTOR
     * =================================================== */

    var _init = function () {
        _subscribers = {};
    }();

};

// Global shared variable
var notificationCenter = notificationCenter || new NotificationCenter();